#!/usr/bin/env python
# -*- coding: utf-8 -*-

import __builtin__
import collections
import imp
import marshal
import os
import re
import struct


MAGIC = imp.get_magic()


_PYTHON_IDENT = r'[_A-Za-z][_A-Za-z1-9]*'
PYTHON_IDENT = _PYTHON_IDENT + r'(?:\.%s)*' % _PYTHON_IDENT

PYTHON_INT = r'-?\d+'
PYTHON_FLOAT = r'-?([0-9]*\.[0-9]+|[0-9]+\.)'
PYTHON_NUM = r'(?:%s)|(?:%s)' % (PYTHON_FLOAT, PYTHON_INT)


PYTHON_KEY = r'(?:%s|%s)' % (PYTHON_INT, PYTHON_IDENT)
PYTHON_SLICE = r'%s*:%s*' % (PYTHON_KEY, PYTHON_KEY)
PYTHON_GETITEM = r'\[(?:%s|%s|[\'"].+[\'"])\]' % (PYTHON_KEY, PYTHON_SLICE)
PYTHON_STRING = r'(?:[\'"].+[\'"])'

PYTHON_FUNC_ARG = r'%s|%s|%s' % (PYTHON_STRING, PYTHON_NUM, PYTHON_IDENT)
PYTHON_FUNC_SIG = r'\s*(?:%s(?:=(?:%s))?)\s*?,?' % (PYTHON_FUNC_ARG,
                                                    PYTHON_FUNC_ARG)
PYTHON_FUNC_CALL = r'\((?:%s)*\)' % PYTHON_FUNC_SIG

_COMPARISON = r'in|<=|>=|==|<|>'
_EXPRESSION = r'%s(?:(?:%s)|(?:%s))?' % (PYTHON_IDENT,
                                         PYTHON_GETITEM,
                                         PYTHON_FUNC_CALL)

PYTHON_IF = (r'if\s+(?:%s)(?:\s+(?:%s)\s+(?:(?:%s)|%s|%s))?' % (_EXPRESSION,
                                                                _COMPARISON,
                                                                _EXPRESSION,
                                                                PYTHON_STRING,
                                                                PYTHON_NUM))
PYTHON_FOR = (r'for\s+(?:%s)(?:\s*,\s*(?:%s))?\s+in\s+(?:%s)(?:\s+%s)?' %
              (_PYTHON_IDENT, _PYTHON_IDENT, _EXPRESSION, PYTHON_IF))

MACRO = r'macro\s+(?:%s)\('

EXPRESSION = re.compile(r'\{\{'
                        r'(?P<lneg>-)?'
                        r'\s+'
                        r'(?P<expression>%s)'
                        r'\s+'
                        r'(?P<rneg>-)?'
                        r'\}\}' % _EXPRESSION)


# NOTE(jkoelker) From jinja2.defaults
BLOCK_START_STRING = '{%'
BLOCK_END_STRING = '%}'
VARIABLE_START_STRING = '{{'
VARIABLE_END_STRING = '}}'
COMMENT_START_STRING = '{#'
COMMENT_END_STRING = '#}'

# NOTE(jkoelker) from jinja2.lexer
TOKEN_ADD = intern('add')
TOKEN_ASSIGN = intern('assign')
TOKEN_COLON = intern('colon')
TOKEN_COMMA = intern('comma')
TOKEN_DIV = intern('div')
TOKEN_DOT = intern('dot')
TOKEN_EQ = intern('eq')
TOKEN_FLOORDIV = intern('floordiv')
TOKEN_GT = intern('gt')
TOKEN_GTEQ = intern('gteq')
TOKEN_LBRACE = intern('lbrace')
TOKEN_LBRACKET = intern('lbracket')
TOKEN_LPAREN = intern('lparen')
TOKEN_LT = intern('lt')
TOKEN_LTEQ = intern('lteq')
TOKEN_MOD = intern('mod')
TOKEN_MUL = intern('mul')
TOKEN_NE = intern('ne')
TOKEN_PIPE = intern('pipe')
TOKEN_POW = intern('pow')
TOKEN_RBRACE = intern('rbrace')
TOKEN_RBRACKET = intern('rbracket')
TOKEN_RPAREN = intern('rparen')
TOKEN_SEMICOLON = intern('semicolon')
TOKEN_SUB = intern('sub')
TOKEN_TILDE = intern('tilde')
TOKEN_WHITESPACE = intern('whitespace')
TOKEN_FLOAT = intern('float')
TOKEN_INTEGER = intern('integer')
TOKEN_NAME = intern('name')
TOKEN_STRING = intern('string')
TOKEN_OPERATOR = intern('operator')
TOKEN_BLOCK_BEGIN = intern('block_begin')
TOKEN_BLOCK_END = intern('block_end')
TOKEN_VARIABLE_BEGIN = intern('variable_begin')
TOKEN_VARIABLE_END = intern('variable_end')
TOKEN_RAW_BEGIN = intern('raw_begin')
TOKEN_RAW_END = intern('raw_end')
TOKEN_COMMENT_BEGIN = intern('comment_begin')
TOKEN_COMMENT_END = intern('comment_end')
TOKEN_COMMENT = intern('comment')
TOKEN_DATA = intern('data')
TOKEN_INITIAL = intern('initial')
TOKEN_EOF = intern('eof')


TOKEN_NEWLINE = intern('newline')
TOKEN_FOR = intern('for')
TOKEN_ENDFOR = intern('endfor')
TOKEN_IF = intern('if')
TOKEN_ELSE = intern('else')
TOKEN_ELIF = intern('elif')
TOKEN_ENDIF = intern('endif')
TOKEN_IN = intern('in')
TOKEN_BLOCK = intern('block')
TOKEN_ENDBLOCK = intern('endblock')
TOKEN_SET = intern('set')
TOKEN_NOT = intern('not')
TOKEN_EXTENDS = intern('extends')
TOKEN_MACRO = intern('macro')
TOKEN_INCLUDE = intern('include')
TOKEN_IMPORT = intern('import')
TOKEN_FROM = intern('from')
SPACE = intern(' ')


def trie(names):
    result = {}

    for name in names:
        tree = result

        for char in name:
            tree = tree.setdefault(char, {})

        tree = tree.setdefault(SPACE, name)

    return result


# NOTE(jkoelker) Masquerade the same exception names as jinja2
class TemplateError(Exception):
    pass


class TemplateSyntaxError(TemplateError):
    def __init__(self, message, lineno, name=None, filename=None,
                 position=None):
        self.lineno = lineno
        self.name = name
        self.filename = filename
        self.position = position

        name = filename or name
        location = 'line %d' % lineno

        if position is not None:
            location = location + ' (column %d)' % position

        if name:
            location = 'File "%s", %s' % (name, location)

        message = '\n'.join([message, '  ' + location])
        TemplateError.__init__(self, message)


class Lexer(object):
    # NOTE(jkoelker) from jinja2.lexer
    OPERATORS = {
        '+': TOKEN_ADD,
        '-': TOKEN_SUB,
        '/': TOKEN_DIV,
        '//': TOKEN_FLOORDIV,
        '*': TOKEN_MUL,
        '%': TOKEN_MOD,
        '**': TOKEN_POW,
        '~': TOKEN_TILDE,
        '[': TOKEN_LBRACKET,
        ']': TOKEN_RBRACKET,
        '(': TOKEN_LPAREN,
        ')': TOKEN_RPAREN,
        '{': TOKEN_LBRACE,
        '}': TOKEN_RBRACE,
        '==': TOKEN_EQ,
        '!=': TOKEN_NE,
        '>': TOKEN_GT,
        '>=': TOKEN_GTEQ,
        '<': TOKEN_LT,
        '<=': TOKEN_LTEQ,
        '=': TOKEN_ASSIGN,
        '.': TOKEN_DOT,
        ':': TOKEN_COLON,
        '|': TOKEN_PIPE,
        ',': TOKEN_COMMA,
        ';': TOKEN_SEMICOLON
    }

    TOKEN_OPERATORS = dict([(v, k) for k, v in OPERATORS.iteritems()])
    STRING_CHARS = ('"', "'")
    CHAR_PEEKS = ('/', '*', '=', '!', '>', '<')
    BLOCK_PEEKS = ('{', '}', '%', '#')
    BLOCK_STATES = (TOKEN_BLOCK_BEGIN, TOKEN_VARIABLE_BEGIN,
                    TOKEN_COMMENT_BEGIN)
    END_STATES = [TOKEN_BLOCK_END, TOKEN_VARIABLE_END, TOKEN_COMMENT_END]
    NAMES = (TOKEN_FOR, TOKEN_ENDFOR, TOKEN_IF, TOKEN_ELSE, TOKEN_ELIF,
             TOKEN_ENDIF, TOKEN_IN, TOKEN_ENDIF, TOKEN_BLOCK, TOKEN_ENDBLOCK,
             TOKEN_SET, TOKEN_NOT, TOKEN_EXTENDS, TOKEN_MACRO, TOKEN_INCLUDE,
             TOKEN_IMPORT, TOKEN_FROM)
    NAMES_TRIE = trie(NAMES)

    COMMENT_STATES = (TOKEN_COMMENT_BEGIN, TOKEN_COMMENT, TOKEN_COMMENT_END)
    BLOCKS = {BLOCK_START_STRING: TOKEN_BLOCK_BEGIN,
              VARIABLE_START_STRING: TOKEN_VARIABLE_BEGIN,
              COMMENT_START_STRING: TOKEN_COMMENT_BEGIN,
              BLOCK_END_STRING: TOKEN_BLOCK_END,
              VARIABLE_END_STRING: TOKEN_VARIABLE_END,
              COMMENT_END_STRING: TOKEN_COMMENT_END}

    NEWLINE = '\n'

    def __init__(self, filename=None):
        self.filename = filename
        self._state = TOKEN_DATA
        self._prev_state = None
        self._line = 1
        self._pos = 1
        self._skip = False
        self._skip_ahead = 0

    def token(self, token, pos=None, data=None):
        return dict(token=token,
                    line=self._line,
                    position=pos,
                    data=data)

    def tokenize(self, stream=None):
        if stream is None:
            try:
                f = open(self.filename, 'r')
                stream = f.read()
            finally:
                f.close()

        data_buffer = []
        add_data = data_buffer.append
        for pos, char in enumerate(stream):
            self._pos = self._pos + 1
            if self._skip:
                if char == self.NEWLINE:
                    self._line = self._line + 1
                    self._pos = 1

                self._skip = False
                continue

            elif self._skip_ahead > 0:
                self._skip_ahead = self._skip_ahead - 1
                if char == self.NEWLINE:
                    self._line = self._line + 1
                    self._pos = 1

                continue

            elif self._state in self.END_STATES:
                self._state = TOKEN_DATA

            if char == self.NEWLINE:
                self._line = self._line + 1
                if data_buffer:
                    yield self.token(TOKEN_DATA, None, ''.join(data_buffer))
                    data_buffer[:] = []

                yield self.token(TOKEN_NEWLINE, self._pos, char)
                self._pos = 1

            elif char in self.BLOCK_PEEKS:
                tok = char + stream[pos + 1]
                state = self.BLOCKS.get(tok)

                if state is not None:
                    if data_buffer and self._state == TOKEN_DATA:
                        yield self.token(TOKEN_DATA, None,
                                         ''.join(data_buffer))
                        data_buffer[:] = []

                    self._state = state
                    self._skip = True
                    yield self.token(state, self._pos)
                    continue

            elif self._state == TOKEN_DATA:
                add_data(char)

            if self._state in self.BLOCK_STATES:
                if char in self.NAMES_TRIE:
                    name = self.NAMES_TRIE[char]
                    next_char = char
                    while next_char != SPACE:
                        next_char = stream[pos + self._skip_ahead + 1]
                        self._skip_ahead = self._skip_ahead + 1
                        name = name.get(next_char)

                        if name is None:
                            self._skip_ahead = 0
                            break

                    if name:
                        yield self.token(name, self._pos, name)
                        continue

                if char in self.CHAR_PEEKS:
                    tok = char + stream[pos + 1]
                    op = self.OPERATORS.get(tok)

                    if op is not None:
                        self._skip = True
                        yield self.token(op, pos, tok)

                    elif char in self.OPERATORS:
                        yield self.token(self.OPERATORS[char], self._pos, char)

                elif char in self.OPERATORS:
                    yield self.token(self.OPERATORS[char], self._pos, char)

                elif char in self.STRING_CHARS:
                    self._prev_state = self._state
                    self._state = char

                elif self._state not in self.COMMENT_STATES:
                    next_char = char
                    while next_char != SPACE:
                        add_data(next_char)
                        next_char = stream[pos + self._skip_ahead + 1]
                        self._skip_ahead = self._skip_ahead + 1

                    if data_buffer:
                        yield self.token(TOKEN_NAME, self._pos,
                                         ''.join(data_buffer))
                        data_buffer[:] = []

            elif self._state in self.STRING_CHARS:
                next_char = char
                while next_char != self._state:
                    add_data(next_char)
                    next_char = stream[pos + self._skip_ahead + 1]
                    self._skip_ahead = self._skip_ahead + 1

                self._state = self._prev_state
                self._prev_state = None

                if data_buffer:
                    yield self.token(TOKEN_STRING, self._pos,
                                     ''.join(data_buffer))
                    data_buffer[:] = []

        if data_buffer:
            yield self.token(TOKEN_DATA, None, ''.join(data_buffer))


class TokenIter(object):
    def __init__(self, stream):
        self._stream = stream
        self._tokens = collections.deque()
        self._stop = False
        self._callbacks = {}

    def __iter__(self):
        return self

    def stop(self):
        self._stop = True

    def start(self):
        self._stop = False

    def add_callback(self, token, func):
        if token not in self._callbacks:
            self._callbacks[token] = collections.deque()

        self._callbacks[token].append(func)

    def next(self):
        if self._stop:
            raise StopIteration()

        if self._tokens:
            return self._tokens.popleft()

        token = self._stream.next()

        callbacks = self._callbacks.get(token['token'])

        if callbacks:
            func = callbacks.popleft()
            func(self, token)

        return token

    def peek(self):
        try:
            token = self._stream.next()
        except StopIteration:
            return

        self._tokens.append(token)
        return token


class Parser(object):
    STATEMENTS = (TOKEN_FOR, TOKEN_ENDFOR, TOKEN_IF, TOKEN_ELSE, TOKEN_ELIF,
                  TOKEN_ENDIF, TOKEN_BLOCK, TOKEN_EXTENDS, TOKEN_MACRO,
                  TOKEN_INCLUDE, TOKEN_IMPORT, TOKEN_SET)
    COMPARISONS = (TOKEN_EQ, TOKEN_NE, TOKEN_LT, TOKEN_LTEQ, TOKEN_GT,
                   TOKEN_GTEQ)

    def __init__(self, filename=None):
        self.filename = filename
        self._lstrip = False
        self._rstrip = False

    def _at(self, token=None):
        ret = {'lineno': 0,
               'position': None,
               'filename': self.filename}

        if token is not None:
            ret['lineno'] = token['line']
            ret['position'] = token['position']

        return ret

    def _perhaps_strip_previous(self, stream):
        peek = stream.peek()

        if peek is None:
            pass  # TODO(jkoelker) handle

        if peek['token'] == TOKEN_SUB:
            peek = stream.next()
            # TODO(jkoelker) if previous is data strip it

    def parse(self, stream=None):
        tree = []
        add_node = tree.append

        if stream is None:
            stream = Lexer(self.filename).tokenize()

        if not isinstance(stream, TokenIter):
            stream = TokenIter(stream)

        #return list(stream)
        for token in stream:
            node = self.handle_token(token, stream)

            if node:
                add_node(node)

        return tree

    def handle_token(self, token, stream, **parse_kwargs):
        handler = getattr(self, '_'.join(('handle', token['token'])), None)
        print token
        if handler is not None:
            return handler(token, stream, **parse_kwargs)

    def handle_data(self, token, stream, **parse_kwargs):
        data = token['data']

        if self._lstrip and self._rstrip:
            data = data.strip()

        elif self._lstrip:
            data = data.lstrip()

        elif self._rstrip:
            data = data.rstrip()

        return data

    def handle_block_begin(self, token, stream):
        self._perhaps_strip_previous(stream)
        statement = stream.next()

        if statement['token'] not in self.STATEMENTS:
            msg = 'Expected one of %s, got %s' % (self.STATEMENTS,
                                                  statement['token'])
            raise TemplateSyntaxError(msg, **self._at(statement))

        stream.add_callback(TOKEN_BLOCK_END, lambda s, t: s.stop())
        block = self.parse(stream=stream)
        stream.start()

        parser = getattr(self, '_'.join(('parse', statement['token'])), None)
        if parser is not None:
            return parser(block, stream)

    def handle_variable_begin(self, token, stream):
        self._perhaps_strip_previous(stream)
        stream.add_callback(TOKEN_VARIABLE_END, lambda s, t: s.stop())
        block = self.parse(stream=stream)
        stream.start()
        return {'wariable': block}

    def handle_in(self, token, stream):
        return TOKEN_IN

    def handle_if(self, token, stream):
        return TOKEN_IF

    def parse_for(self, block, stream):
        return {TOKEN_FOR: block}

#    def handle_for(self, token, stream):
#        self._perhaps_strip_previous(stream)
#        stream.add_callback(TOKEN_ENDFOR, lambda s, t: s.stop())
#        block = self.parse(stream=stream)
#        stream.start()
#        return {'for': block}

    def handle_name(self, token, stream):
        return token['data']


class Template(object):

    def __init__(self, name=None):
        if name is None:
            name = '<Template>'

        self.name = name
        self.codestring = None

    def compile(self):
        if self.codestring is None:
            self.parse()

        if self.codestring and self.codestring[-1] != '\n':
            self.codestring = '%s\n' % self.codestring

        return __builtin__.compile(self.codestring, self.name, 'exec')

    def parse(self):
        f = open(self.name, 'r')
        stream = f.read()
        f.close()
        return stream


class FileTemplate(Template):
    NONE = object()

    def __init__(self, name):
        name = os.path.abspath(name)
        Template.__init__(self, name=name)

    def _code_filename(self):
        name, ext = os.path.splitext(self.name)
        return '%s.jo' % name

    def _perhaps_load(self, timestamp):
        filename = self._code_filename()

        try:
            f = open(filename, 'rb')

            try:
                fread = f.read
                if fread(4) == MAGIC:
                    code_timestamp = fread(4)
                    code_timestamp = struct.unpack('<I', code_timestamp)[0]
                    if code_timestamp == timestamp:
                        return marshal.load(f)

            finally:
                f.close()

        except:
            pass

        return self.NONE

    def _compile(self, timestamp):
        codeobject = Template.compile(self)
        filename = self._code_filename()

        try:
            f = open(filename, 'wb')

            try:
                f.write('\0\0\0\0')
                f.write(struct.pack('<I', timestamp))
                marshal.dump(codeobject, f)
                f.flush()
                f.seek(0, 0)
                f.write(MAGIC)

            finally:
                f.close()

        except:
            pass

        return codeobject

    def compile(self):
        timestamp = long(os.stat(self.name).st_mtime)

        codeobject = self._perhaps_load(timestamp)

        if codeobject is self.NONE:
            codeobject = self._compile(timestamp)

        return codeobject


class Environment(object):
    pass


if __name__ == '__main__':
    try:
        import json
    except ImportError:
        import simplejson as json

    import getopt
    import sys

    usage = '''
    jinjish.py [-s] <-d NAME=VALUE> <-f DATAFILE> FILES
    -s      syntax-check
    -d      define variables
    -f      json file of variables
    '''.strip()

    try:
        opt_list, files = getopt.getopt(sys.argv[1:], 'sd:f:h', ('help'))
    except getopt.GetoptError, err:
        sys.stderr.write('ERROR: Invalid option. (%s)\n' % err)
        sys.exit(3)

    if not files:
        sys.stdout.write('%s\n' % usage)
        sys.exit(1)

    datafile = None
    render = True
    file_variables = None
    cli_variables = {}

    for key, value in opt_list:
        if key in ('-h', '--help'):
            sys.stdout.write('%s\n' % usage)
            sys.exit()

        elif key == '-s':
            render = False

        elif key == '-d':
            name, value = value.split('=', 1)
            cli_variables[name] = value

        elif key == '-f':
            if file_variables is not None:
                sys.stderr.write('ERROR: only json file supported\n')
                sys.exit(4)

            f = open(value, 'r')
            file_variables = json.load(f)
            f.close()

    variables = {}

    if file_variables is not None:
        variables.update(file_variables)

    variables.update(cli_variables)

    for template_file in files:
        template = Template(template_file)
        template.parse()
